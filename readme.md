# Cintegrity
Cintegrity is a simple Cinnamon applet for monitoring the status of your IntegrityVPN connection. It is based on the Cinnamond-applet available at https://gitlab.com/eurmal/cinnamond but instead of checking the status of a systemd-service it verifies the connection against the https://api.5july.net/1.0/ipcheck endpoint.

## Installation
1. Copy the entire directory to your Cinnamon applet path. Default being */home/USERNAME/.local/share/cinnamon/applets/*
2. Perform initial configuration by editing the *settings-schema.json*, for information about each setting, see the Configuration section in this readme.
3. Right click your panel and click *Applets*
4. Select the Cintegrity applet and click *Apply*

## Configuration
After the applet is running you can configure it using the cogs-icon in the Cinnamon applet ui. Changes done here will be automatically hot-deployed.
The following settings are available:

| Setting        	| Description           														| Default  					|
| ------------- 	| -------------																	| -------------				|
| update-interval   | How often (in ms) should the status be polled.      							|   5000 					|
| icon-ok 			| Name of the cinnamon icon that should be shown if the service is active.      | channel-secure			|
| icon-nok 			| Name of the cinnamon icon that should be shown if the service is NOT active.	| channel-insecure			|

## Licensing
Cintegrity is free software distributed under the GPLv3 license. For more information, see COPYING.txt.